using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class RaycastInteractable : MonoBehaviour
{
    [SerializeField] string hoverString;
    [SerializeField] private bool interactable;

    [SerializeField] UnityEvent onHoverEnter;
    [SerializeField] UnityEvent onHoverExit;
    [SerializeField] UnityEvent onInteract;

    public void OnHoverEnter()
    {
        onHoverEnter?.Invoke();
    }

    public void OnHoverExit()
    {
        onHoverExit?.Invoke();
    }

    public void OnInteract()
    {
        onInteract?.Invoke();

        //if clicked increment the total picked up by 1
        

    }

    public bool Interactable { get => interactable; set => interactable = value; }
    public string HoverString { get => hoverString; }


// public bool goodFood { get => GoodFood; set => GoodFood = value; }
//    public bool badFood { get => BadFood; set => BadFood = value; }
}
