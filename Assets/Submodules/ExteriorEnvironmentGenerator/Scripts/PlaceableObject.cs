using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlaceableObject 
{
    [SerializeField] private Vector2 perlinRange;
    [SerializeField] private GameObject prefab;

    public Vector2 PerlinRange { get => perlinRange; set => perlinRange = value; }
    public GameObject Prefab { get => prefab; set => prefab = value; }
}
