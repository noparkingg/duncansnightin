using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ExteriorEnvironmentGenerator : MonoBehaviour
{
    [SerializeField] private int seed;
    [SerializeField] private float polygonScale;
    [SerializeField] private int width;
    [SerializeField] private int depth;
    [SerializeField] private float heightScaler;
    [SerializeField] private Material mat;
    [SerializeField] private GameObject tree;

    [SerializeField] private List<RegionGroup> regionGroups;
    [SerializeField] bool addCollider = false;
    
    [SerializeField] private float generationScale = 1;

    Vector3[,] vertexMap;

    private void Awake()
    {
        polygonScale *= generationScale;
        GenerateArea(transform.position);
    }

    public void GenerateArea(Vector3 rootPosition)
    {
        UnityEngine.Random.InitState(seed);

        MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
        meshRenderer.sharedMaterial = mat;

        MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();

        Mesh mesh = new Mesh();
        mesh.vertices = CalculateVertices(rootPosition);
        mesh.triangles = CalculateTriangles();
        mesh.RecalculateNormals();
        mesh.uv = CalculateUVs(mesh.vertices.Length);
        mesh.tangents = CalculateTangents(mesh.vertices.Length);
        meshFilter.mesh = mesh;
        PopulateTerrain();

        if(addCollider)
        {
            gameObject.AddComponent<MeshCollider>();
        }
    }

    private Vector3[] CalculateVertices(Vector3 rootPosition)
    {
        List<Vector3> vertices = new List<Vector3>();


        vertexMap = new Vector3[width + 1, depth + 1];
        for (int i = 0; i <= width; i++)
        {
            for (int j = 0; j <= depth; j++)
            {

                float xBaseOffset = (polygonScale * i);

                float zBaseOffset = (polygonScale * j);
                Vector3 vertex = new Vector3(rootPosition.x + xBaseOffset,
                     Mathf.PerlinNoise(Convert.ToSingle(xBaseOffset) / Convert.ToSingle(width) + seed,
                     Convert.ToSingle(zBaseOffset) / Convert.ToSingle(depth) + seed) * heightScaler,
                     rootPosition.z + zBaseOffset);

                vertices.Add(vertex);
                vertexMap[i, j] = vertex;
            }
        }

        return vertices.ToArray();
    }

    private int[] CalculateTriangles()
    {
        int[] triangles = new int[width * depth * 6];
        for (int ti = 0, vi = 0, y = 0; y < width; y++, vi++)
        {
            for (int x = 0; x < depth; x++, ti += 6, vi++)
            {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + depth + 1;
                triangles[ti + 5] = vi + depth + 2;
            }
        }
        Array.Reverse(triangles);
        return triangles;
    }

    private Vector2[] CalculateUVs(int vertexCount)
    {
        Vector2[] uvs = new Vector2[vertexCount];
        for (int i = 0, y = 0; y <= width; y++)
        {
            for (int x = 0; x <= depth; x++, i++)
            {
                uvs[i] = new Vector2((float)x / width, (float)y / depth);
            }
        }
        return uvs;
        
    }

    private Vector4[] CalculateTangents(int vertexCount)
    {
        Vector4[] tangents = new Vector4[vertexCount];
        Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);
        for (int i = 0, y = 0; y <= width; y++)
        {
            for (int x = 0; x <= depth; x++, i++)
            {
                tangents[i] = tangent;
            }
        }
        return tangents;
    }

    public void PopulateTerrain()
    {
        for (int i = 0; i <= width; i++)
        {
            for (int j = 0; j <= depth; j++)
            {
                float perlinValue = Mathf.PerlinNoise(Convert.ToSingle(i) / Convert.ToSingle(width) + seed,
                     Convert.ToSingle(j) / Convert.ToSingle(depth) + seed);
                Vector3 vertex = vertexMap[i, j];
                PlaceableObject prefab = GetPlaceableObject(perlinValue);
                if (prefab != null)
                {
                    GameObject obj = Instantiate(prefab.Prefab, transform.position + vertex, Quaternion.identity, transform);
                    obj.transform.localScale *= generationScale;
                    obj.transform.Rotate(0, UnityEngine.Random.Range(0f, 360f), 0);
                }

            }
        }
    }

    private PlaceableObject GetPlaceableObject(float perlinValue)
    {
        for(int i = 0; i < regionGroups.Count;i++)
        {
            if(perlinValue > regionGroups[i].PerlinRange.x && perlinValue < regionGroups[i].PerlinRange.y)
            {
                return regionGroups[i].PlaceableGroup.GetPlaceableObject();
            }
        }

        return null;
    }
}
