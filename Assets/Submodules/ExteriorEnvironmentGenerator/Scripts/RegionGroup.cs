using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RegionGroup
{
    [SerializeField] Vector2 perlinRange;
    [SerializeField] PlaceableGroupSO placeableGroup;

    public Vector2 PerlinRange { get => perlinRange; set => perlinRange = value; }
    public PlaceableGroupSO PlaceableGroup { get => placeableGroup; set => placeableGroup = value; }
}
