using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlaceableGroup", menuName = "WorldBuilder/PlaceableGroup")]
public class PlaceableGroupSO : ScriptableObject
{
    [SerializeField] private List<PlaceableObject> placeableObjects;

    public PlaceableObject GetPlaceableObject()
    {
        float randomNum = UnityEngine.Random.Range(0f, 1f);
        

        for (int i = 0; i < placeableObjects.Count; i++)
        {
            if(randomNum > placeableObjects[i].PerlinRange.x && randomNum < placeableObjects[i].PerlinRange.y)
            {
                return placeableObjects[i];
            }
        }

        return null;
    }

    public List<PlaceableObject> PlaceableObjects { get => placeableObjects; set => placeableObjects = value; }
}
