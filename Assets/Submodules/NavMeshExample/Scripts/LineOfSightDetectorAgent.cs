using com.ImmersiveMedia.CharacterControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LineOfSightDetectorAgent : MonoBehaviour
{
    [SerializeField] NavMeshAgent navMeshAgent;

    CharacterInteractable trackedObject;

    private void Update()
    {
        if(trackedObject != null)
        {
            navMeshAgent.SetDestination(trackedObject.transform.position);
        }
    }

    public void OnDetectObject(CharacterInteractable detectedObject)
    {
        trackedObject = detectedObject;
        navMeshAgent.SetDestination(detectedObject.transform.position);
    }
}
