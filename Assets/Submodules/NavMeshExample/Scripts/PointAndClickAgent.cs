using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PointAndClickAgent : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    [SerializeField] NavMeshAgent navMeshAgent;
   

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        if(Input.GetMouseButton(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                Transform objectHit = hit.transform;

                if (hit.transform.gameObject.tag.Equals("Ground"))
                {
                    navMeshAgent.SetDestination(hit.point);
                }
            }
        }
        
    }
}
