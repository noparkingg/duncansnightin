using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is an example for showing the steps of the monobehavior lifecycle
/// </summary>
public class MonobehaviorLifeCycleDemo : MonoBehaviour
{
    private void Awake()
    {
        Debug.Log("Awake");
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start");
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Update");
    }

    private void FixedUpdate()
    {
        Debug.Log("FixedUpdate");
    }

    private void LateUpdate()
    {
        Debug.Log("LateUpdate");
    }

    private void OnDisable()
    {
        Debug.Log("OnDisable");
    }

    private void OnEnable()
    {
        Debug.Log("OnEnable");
    }

    private void OnDestroy()
    {
        Debug.Log("OnDestroy");
    }
}
