using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRSkeletonDeath : MonoBehaviour
{
    public void OnSkeletonDeath()
    {
        Debug.Log("Calling for " + gameObject.name);
        VRGameManager.Instance.OnKillSkeleton();
    }
}
