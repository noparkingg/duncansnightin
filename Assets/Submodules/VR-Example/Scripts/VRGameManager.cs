using com.ImmersiveMedia.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class VRGameManager : MonoBehaviour
{
    public static VRGameManager Instance;

    [SerializeField] VRInfoWindow infoWindow;
    [SerializeField] GameObject spawnerRootTransform;
    [SerializeField] AttackableProvider attackableProvider;
    [SerializeField] AudioClip idleClip;
    [SerializeField] AudioClip actionClip;
    [SerializeField] AudioSource musicPlayer;
    [SerializeField] Transform startTransform;
    [SerializeField] LimitedUseEventProvider crossbowBoltEventProvider;

    private UnityEvent onDialogueComplete;

    private float numberOfHits = 0;
    private float numberOfShots = 0;

    private int skeletonsKilled = 0;

    bool gameStarted = false;

    float gameStartTime;

    Queue<string> messages;


    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(gameObject);
        }
        Instance = this;
        messages = new Queue<string>();
        onDialogueComplete = new UnityEvent();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartGame());
    }

    private IEnumerator StartGame()
    {
        onDialogueComplete?.RemoveAllListeners();
        yield return new WaitForSeconds(2f);
        VRCameraController.Instance.MoveCamera(startTransform.position);
        StartCoroutine(VRCameraController.Instance.FadeInCamera(0.05f, null));
        numberOfHits = 0;
        numberOfShots = 0;
        infoWindow.MoveWindowIntoView();
        messages.Enqueue("Defend your town from the undead!");
        
        List<Action> actions = new List<Action>();
        actions.Add(DisplayNextMessage);
        onDialogueComplete.AddListener(TeleportTutorial);

        infoWindow.CreateButton("Ok", actions);
        DisplayNextMessage();
    }

    private void TeleportTutorial()
    {
        onDialogueComplete?.RemoveAllListeners();
        infoWindow.MoveWindowIntoView();
        messages.Enqueue("Try shooting one of the glowing purple points to teleport around the castle!");
        onDialogueComplete.AddListener(WaitToStartAttack);
        DisplayNextMessage();
        PlayerMoveLocation.onPlayerMove.AddListener(DisplayNextMessage);
        
    }

    private void WaitToStartAttack()
    {
        onDialogueComplete?.RemoveAllListeners();
        Debug.Log("waiting to start attack");
        PlayerMoveLocation.onPlayerMove.RemoveListener(DisplayNextMessage);
        infoWindow.MoveWindowIntoView();
        messages.Enqueue("Great Job!");
        messages.Enqueue("Are your ready to fight the undead horde?");
        DisplayNextMessage();
        List<Action> actions = new List<Action>();
        actions.Add(DisplayNextMessage);
        onDialogueComplete.AddListener(StartAttack);

        infoWindow.CreateButton("Ok", actions);
    }

    private void StartAttack()
    {
        crossbowBoltEventProvider.DepletionActive = true;
        AmmoRefill.SpawnRefills();
        onDialogueComplete?.RemoveAllListeners();
        gameStartTime = Time.time;
        gameStarted = true;
        PerformActionOnInterval[] spawnIntervals = spawnerRootTransform.GetComponentsInChildren<PerformActionOnInterval>();

        foreach (var spawnInterval in spawnIntervals)
        {
            spawnInterval.Active = true;
        }

        musicPlayer.clip = actionClip;
        musicPlayer.Play();
    }

    private void DisplayNextMessage()
    {

        if(messages.Count > 0)
        {
            infoWindow.DisplayMessage(messages.Dequeue());
        }
        else
        {
            infoWindow.DismissMessage();
            onDialogueComplete?.Invoke();
        }
    }

    public void EndGame()
    {
        float elapsedTime = Time.time - gameStartTime;
        int minutes = Mathf.FloorToInt(elapsedTime / 60F);
        int seconds = Mathf.FloorToInt(elapsedTime - minutes * 60);
        string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);
        infoWindow.MoveWindowIntoView();
        int percentage = Math.Max(0, Mathf.FloorToInt((numberOfHits / numberOfShots) * 100));
        infoWindow.DisplayMessage($"Survival Time: {niceTime}\n Skeletons Killed: {skeletonsKilled}\n" +
            $"Accuracy: {percentage}%");

        List<Action> actions = new List<Action>();
        
        actions.Add(() => StartCoroutine(VRCameraController.Instance.FadeOutCamera(0.05f, ReloadScene)));
        infoWindow.CreateButton("Restart", actions);

        gameStarted = false;
        musicPlayer.clip = idleClip;
        musicPlayer.Play();

        
    }

    private void ReloadScene()
    {
        Scene scene = SceneManager.GetActiveScene(); 
        SceneManager.LoadScene(scene.name);
    }

    public void OnKillSkeleton()
    {
        skeletonsKilled++;
    }

    public void ReportShot()
    {
        numberOfShots++;
    }

    public void ReportHit()
    {
        numberOfHits++;
    }

    // Update is called once per frame
    void Update()
    {
        if(attackableProvider.Attackables.Count <= 0 && gameStarted || Input.GetKeyDown(KeyCode.Return))
        {
            EndGame();
        }
    }
}
