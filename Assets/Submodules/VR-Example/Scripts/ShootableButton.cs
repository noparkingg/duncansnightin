using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class ShootableButton : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI buttonText;
    [SerializeField] UnityEvent onHitEvent;
    private List<Action> onHitActions;

    public List<Action> OnHitActions { get => onHitActions; set => onHitActions = value; }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag.Equals("PlayerProjectile") && onHitActions != null)
        {
            OnButtonActivated();
        }
    }

    private void OnButtonActivated()
    {
        foreach (Action action in onHitActions)
        {
            action?.Invoke();
        }
        onHitEvent?.Invoke();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            OnButtonActivated();
        }
    }

    public void InitializeButton(string text, List<Action> actions)
    {
        OnHitActions = actions;
        if (buttonText != null)
        {
            buttonText.text = text;
        }
    }

    private void OnDestroy()
    {
        onHitEvent?.RemoveAllListeners();
    }
}
