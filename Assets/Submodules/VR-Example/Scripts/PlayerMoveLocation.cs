using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerMoveLocation : MonoBehaviour
{
    [SerializeField] GameObject marker;

    public static UnityEvent onPlayerMove;

    [SerializeField] UnityEvent onPlayerArrive;

    private static int currentMoveLocationID = -1;

    [SerializeField] bool initialLocation = false;



    private void Awake()
    {
        if(initialLocation)
        {
            currentMoveLocationID = gameObject.GetInstanceID();
            marker.SetActive(false);
        }

        if(onPlayerMove == null)
        {
            onPlayerMove = new UnityEvent();
        }
        onPlayerMove.AddListener(OnPlayerMove);
    }

    private void OnPlayerMove()
    {
        if(currentMoveLocationID != gameObject.GetInstanceID())
        {
            marker.SetActive(true);
        }
        else
        {
            marker.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("PlayerProjectile") && currentMoveLocationID != gameObject.GetInstanceID())
        {
            VRCameraController.Instance.MoveCamera(transform.position);
            currentMoveLocationID = gameObject.GetInstanceID();
            other.gameObject.SetActive(false);
            onPlayerMove.Invoke();
            onPlayerArrive.Invoke();
        }
    }
}
