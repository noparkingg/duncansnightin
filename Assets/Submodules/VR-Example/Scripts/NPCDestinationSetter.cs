using com.ImmersiveMedia.CharacterControl;
using static com.ImmersiveMedia.Enums.InteractionEnums;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCDestinationSetter : MonoBehaviour
{

    public void RegisterDestinationSetterToNPC(GameObject npcObject)
    {
        NPCController controller = npcObject.GetComponent<NPCController>();

        if (controller != null)
        {
            controller.OnInteractionComplete.AddListener(SetNPCDestination);
            controller.OnDeath.AddListener(VRGameManager.Instance.OnKillSkeleton);
        }
    }

    public void SetNPCDestination(GameObject npcObject)
    {
        NPCController controller = npcObject.GetComponent<NPCController>();

        if (controller != null)
        {
            CharacterInteractable startingGate = AttackableProvider.Instance.GetStartingGate(transform);

            if(startingGate.InteractionOption != CharacterInteraction.NOT_INTERACTABLE)
            {
                controller.SetInteraction(AttackableProvider.Instance.GetStartingGate(transform));
            }
            else
            {
                CharacterInteractable attackable = AttackableProvider.Instance.GetAttackable();
                if(attackable != null)
                {
                    controller.SetInteraction(attackable);
                }

            }

            
        }
    }
}
