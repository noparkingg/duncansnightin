using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccuracyReporter : MonoBehaviour
{
    public void ReportHit()
    {
        VRGameManager.Instance.ReportHit();
    }

    public void ReportShot()
    {
        VRGameManager.Instance.ReportShot();
    }
    
}
