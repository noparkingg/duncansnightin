using com.ImmersiveMedia.CharacterControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackableProvider : MonoBehaviour
{
    public static AttackableProvider Instance;

    [SerializeField] private List<CharacterInteractable> gates;
    [SerializeField] private List<CharacterInteractable> attackables;

    public List<CharacterInteractable> Attackables { get => attackables;}

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(gameObject);
        }

        Instance = this;
    }

    public CharacterInteractable GetStartingGate(Transform spawnLocation)
    {
        CharacterInteractable closestGate = null;

        foreach(var gate in gates)
        {
            if(closestGate == null || Vector3.Distance(spawnLocation.position, closestGate.transform.position) 
                > Vector3.Distance(spawnLocation.position, gate.transform.position))
            {
                closestGate = gate;
            }
        }

        return closestGate;
    }

    public void RegisterAttackable(CharacterInteractable interactable)
    {
        Attackables.Add(interactable);
    }

    public void RemoveAttackable(CharacterInteractable interactable)
    {
        for(int i = 0; i < Attackables.Count; i++)
        {
            if(Attackables[i].GetInstanceID() == interactable.GetInstanceID())
            {
                Attackables.RemoveAt(i);
                return;
            }
        }
    }

    public CharacterInteractable GetAttackable()
    {
        if(attackables.Count == 0)
        {
            return null;
        }
        int randomIndex = UnityEngine.Random.Range(0, Attackables.Count);
        return Attackables[randomIndex];
    }

    
}
