using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class AmmoRefill : MonoBehaviour
{
    [SerializeField] int amount;
    [SerializeField] private UnityEvent<int> onRefillAmmo;
    [SerializeField] UnityEvent onActivated;
    [SerializeField] UnityEvent onDeativated;

    private static int maxActiveRefills = 3;

    private static bool listsInitialized = false;
    private static List<AmmoRefill> inactiveAmmoRefills;
    private static List<AmmoRefill> activeAmmoRefills;

    private bool activated = false;

    private bool initialSpawnComplete = false;

    private void Awake()
    {
        if(!listsInitialized)
        {
            inactiveAmmoRefills = new List<AmmoRefill>();
            activeAmmoRefills = new List<AmmoRefill>();
            listsInitialized = true;
        }
        inactiveAmmoRefills.Add(this); 
    }

    private void Start()
    {
        if(!initialSpawnComplete)
        {
            initialSpawnComplete = true;
        }
    }

    public static void SpawnRefills()
    {
        while (activeAmmoRefills.Count < maxActiveRefills)
        {
            int index = Random.Range(0, inactiveAmmoRefills.Count);
            AmmoRefill ammoRefill = inactiveAmmoRefills[index];
            ammoRefill.activated = true;
            inactiveAmmoRefills.Remove(ammoRefill);
            activeAmmoRefills.Add(ammoRefill);
            ammoRefill.onActivated?.Invoke();
        }
    }

    public void OnRefill()
    {
        if(activated)
        {
            onRefillAmmo?.Invoke(amount);
            onDeativated?.Invoke();
            activeAmmoRefills.Remove(this);
            inactiveAmmoRefills.Add(this);
            this.activated = false;
            SpawnRefills();
        }
    }

    private void OnDestroy()
    {
        inactiveAmmoRefills = null;
        activeAmmoRefills = null;
        listsInitialized = false;
    }
}
