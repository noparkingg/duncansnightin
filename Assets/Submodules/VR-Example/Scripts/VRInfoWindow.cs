using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class VRInfoWindow : MonoBehaviour
{
    [SerializeField] GameObject root;
    [SerializeField] TextMeshProUGUI textObject;
    [SerializeField] float headOffset;

    [SerializeField] Transform head;
    [SerializeField] GameObject buttonPrefab;
    [SerializeField] float buttonSpacing;
    [SerializeField] Transform buttonTransform;

    [SerializeField] UnityEvent onDisplayMessage;

    List<GameObject> buttons;

    private void Awake()
    {
        buttons = new List<GameObject>();
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
    }

    public void DisplayMessage(string message)
    {
        root.SetActive(true);
        textObject.text = message;
        onDisplayMessage?.Invoke();
    }

    public void DismissMessage()
    {
        root.SetActive(false);
        ClearButtons();
    }

    public void MoveWindowIntoView()
    {
        transform.position = head.transform.position + new Vector3(head.forward.x, 0f, head.forward.z) * headOffset;
        transform.LookAt(new Vector3(head.position.x, transform.position.y, head.position.z));
    }

    public void CreateButton(string text, List<Action> actions)
    {
        GameObject button = Instantiate(buttonPrefab, buttonTransform);
        ShootableButton sButton = button.GetComponent<ShootableButton>();

        sButton.InitializeButton(text, actions);
        buttons.Add(button);
    }

    public void ClearButtons()
    {
        foreach(GameObject button in buttons)
        {
            Destroy(button);
        }

        buttons.Clear();
    }
}
