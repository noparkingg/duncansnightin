using com.ImmersiveMedia.CharacterControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attackable : MonoBehaviour
{
    [SerializeField] CharacterInteractable attackableInteractable;

    private void Start()
    {
        AttackableProvider.Instance.RegisterAttackable(attackableInteractable);
    }

    public void OnAttackableDestroyed()
    {
        AttackableProvider.Instance.RemoveAttackable(attackableInteractable);
    }
}
