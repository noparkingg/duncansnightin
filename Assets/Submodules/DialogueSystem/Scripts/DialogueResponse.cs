using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Example.DialogueSystem
{
    /// <summary>
    /// A response to dialog that is packaged with the next dialogue choice set
    /// </summary>
    [Serializable]
    public class DialogueResponse
    {
        [SerializeField] string responseString; // The string response for the user to say
        [SerializeField] KeyCode inputKey; // Key the user needs to press to choose the dialog
        [SerializeField] DialogueChoiceSet destinationSet; // The choice set that will be loaded as a result of choosing this response

        public DialogueChoiceSet DestinationSet { get => destinationSet;}
        public string DialogueString { get => responseString;}
        public KeyCode ChoiceKey { get => inputKey; set => inputKey = value; }
    }
}