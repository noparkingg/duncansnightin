using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Example.DialogueSystem
{
    /// <summary>
    /// Inteprets dialog data objects and displays dialog to the user
    /// </summary>
    public class DialogueController : MonoBehaviour
    {

        [SerializeField] Text dialogueText; // The current dialogue being displayed to the user
        [SerializeField] List<Text> responseTextObjects; // The current responses the user can give

        [SerializeField] UnityEvent onDialogueCompleteEvent;

        List<Tuple<KeyCode, Action>> choiceEvents;

        [SerializeField] GameObject board;

        private void Update()
        {
            // Loop throught the callback events and check for the user's response
            if(choiceEvents != null)
            {
                for(int i = 0; i < choiceEvents.Count; i++)
                {
                    choiceEvents[i].Item2(); // Invoke the dialogue choice callback the user selected
                }
            }
        }

        public void SetDialogueTree(DialogueTree tree)
        {
            SetDialogueChoiceSet(tree.RootDialogueChoiceSet);
            board.SetActive(true);
            
        }

        /// <summary>
        /// Updates the the dialogue and responses displyed to the user
        /// </summary>
        /// <param name="choiceSet">A set of dialogue and responses to that dialogue the user can choose</param>
        private void SetDialogueChoiceSet(DialogueChoiceSet choiceSet)
        {
            // If the choiceSet is null we have reached the end of this dialogue tree
            if(choiceSet == null)
            {
                onDialogueCompleteEvent?.Invoke();
                
                board.SetActive(false);
                return;
            }

            dialogueText.text = choiceSet.DialogueString; // Update the dialogue string in the dialogue window
            choiceEvents = new List<Tuple<KeyCode, Action>>();
            
            // Iterate through all the dialogue choices and populate the dialogue window
            for(int i = 0; i < choiceSet.DialogueChoices.Count; i++)
            {
                // Stop iterating when we run out of text objects to assign dialogue to
                if(i > responseTextObjects.Count)
                {
                    break;
                }

                DialogueResponse response = choiceSet.DialogueChoices[i]; 

                // Generate the response string and append the input keycode to it
                responseTextObjects[i].text = $"{response.DialogueString} ({response.ChoiceKey}).";

                // Assign the next dialogue choice to set to the callback
                Action selectionAction = () =>
                {   
                    if(Input.GetKeyDown(response.ChoiceKey))
                    {
                        SetDialogueChoiceSet(response.DestinationSet);
                    }
                };

                Tuple<KeyCode, Action> eventTuple = new Tuple<KeyCode, Action>(response.ChoiceKey, selectionAction);
                choiceEvents.Add(eventTuple);
            }
        }
    }
}

