using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Example.DialogueSystem
{
    /// <summary>
    /// A data object to hold a piece of dialogue 
    /// and the responses a user can make to that
    /// piece of dialogue 
    /// </summary>
    [CreateAssetMenu(fileName = "DialogueChoiceSet", menuName = "DialogueSystem/DialogueChoiceSet")]
    public class DialogueChoiceSet : ScriptableObject
    {
        [SerializeField] string dialogueString; // The dialogue string for the user to reply to
        [SerializeField] List<DialogueResponse> dialogueResponses; // The responses the user can make to the given dialogue

        public List<DialogueResponse> DialogueChoices { get => dialogueResponses; set => dialogueResponses = value; }
        public string DialogueString { get => dialogueString; set => dialogueString = value; }
    }
}
