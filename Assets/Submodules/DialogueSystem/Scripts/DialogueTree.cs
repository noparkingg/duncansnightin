using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Example.DialogueSystem
{
    /// <summary>
    /// Data object that holds the root dialog node
    /// </summary>
    [CreateAssetMenu(fileName = "DialogueTree", menuName = "DialogueSystem/DialogueTree")]
    public class DialogueTree : ScriptableObject
    {
        [SerializeField] private DialogueChoiceSet rootDialogueChoiceSet;

        public DialogueChoiceSet RootDialogueChoiceSet { get => rootDialogueChoiceSet; set => rootDialogueChoiceSet = value; }
    }
}
