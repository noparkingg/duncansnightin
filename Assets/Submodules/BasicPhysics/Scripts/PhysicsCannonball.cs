using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsCannonball : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    [SerializeField] float forceScaler;
    [SerializeField] float persistTime;

    private void Awake()
    {
        rb.AddForce(transform.forward * forceScaler, ForceMode.Impulse);
        StartCoroutine(DestroyAfterDelay());
    }

    private IEnumerator DestroyAfterDelay()
    {
        yield return new WaitForSeconds(persistTime);
        Destroy(gameObject);
    }
}
