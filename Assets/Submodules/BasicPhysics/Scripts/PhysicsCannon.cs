using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsCannon : MonoBehaviour
{
    [SerializeField] GameObject cannonballPrefab;
    [SerializeField] Transform spawnPoint;
    [SerializeField] Transform rotationBase;
    [SerializeField] float rotationSpeed;
    [SerializeField] AudioSource audioSource;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(audioSource.isPlaying)
            {
                audioSource.Stop();
            }
            audioSource.Play();
            Instantiate(cannonballPrefab, spawnPoint.position, spawnPoint.rotation);
        }

        float rotationAmount = Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;

        transform.Rotate(new Vector3(0, rotationAmount, 0));
    }
}
