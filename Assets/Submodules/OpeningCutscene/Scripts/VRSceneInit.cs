using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRSceneInit : MonoBehaviour
{
    [SerializeField] Transform startTransform;
    // Start is called before the first frame update
    void Start()
    {
        VRCameraController.Instance.MoveCamera(startTransform.position);
        StartCoroutine(VRCameraController.Instance.FadeInCamera(0.05f, null));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
