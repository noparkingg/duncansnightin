using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Timeline;

public class CutsceneEventProvider : MonoBehaviour, ITimeControl
{
    [SerializeField] List<UnityEvent> onControlStartedEvents;
    [SerializeField] List<UnityEvent> onControlStoppedEvents;

    public void OnControlTimeStart()
    {
        foreach(UnityEvent evt in onControlStartedEvents)
        {
            evt?.Invoke();
        }
    }

    public void OnControlTimeStop()
    {
        foreach (UnityEvent evt in onControlStoppedEvents)
        {
            evt?.Invoke();
        }
    }

    public void SetTime(double time)
    {
        // Do nothing
    }

    
}
