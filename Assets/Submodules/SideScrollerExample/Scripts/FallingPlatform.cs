using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Platform that falls when the user jumps on it
/// </summary>
public class FallingPlatform : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag.Equals("Player"))
        {
            rb.useGravity = true;
        }
    }
}
