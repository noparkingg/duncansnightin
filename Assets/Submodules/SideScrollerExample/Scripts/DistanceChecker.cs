using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DistanceChecker : MonoBehaviour
{
    [SerializeField] private float maxDistance;
    [SerializeField] private float checkInterval;

    [SerializeField] private Transform objectOne;
    [SerializeField] private Transform objectTwo;

    [SerializeField] private UnityEvent onMaxDistanceExceeded;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DistanceCheckRoutine());
    }

    private IEnumerator DistanceCheckRoutine()
    {
        while(Vector3.Distance(objectOne.position, objectTwo.position) < maxDistance)
        {
            yield return new WaitForSeconds(checkInterval);
        }

        onMaxDistanceExceeded?.Invoke();
    }
}
