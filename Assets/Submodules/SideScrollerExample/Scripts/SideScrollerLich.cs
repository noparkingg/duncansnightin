using com.ImmersiveMedia.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SideScrollerLich : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    [SerializeField] float speed;

    [SerializeField] Animator anim;
    [SerializeField] Transform projectileSpawnTransform;

    [SerializeField] UnityEvent onFireProjectile;

    [SerializeField] UnityEvent onGateReached;

    Pooler projectilePooler;

    private bool movementActive = false;
    private bool attacking = false;

    public bool Attacking { get => attacking; set => attacking = value; }

    // Start is called before the first frame update
    void Start()
    {
        projectilePooler = PoolManager.Instance.GetPool("LichBolt");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Attack()
    {
        anim.SetTrigger("Attack");
    }

    public void FireProjectile()
    {
        GameObject projectile = projectilePooler.GetPooledObject();

        projectile.transform.position = projectileSpawnTransform.position;
        projectile.transform.rotation = projectileSpawnTransform.rotation;
        projectile.SetActive(true);
        onFireProjectile?.Invoke();
    }

    public void ActivateAttackMode()
    {
        movementActive = true;
        Attacking = true;
        StartCoroutine(AttackRoutine());
    }

    public void DeactivateAttackMode()
    {
        movementActive = false;
        Attacking = false;
    }

    private IEnumerator AttackRoutine()
    {
        while(Attacking)
        {
            Attack();
            float randomInterval = UnityEngine.Random.Range(1, 5);
            yield return new WaitForSeconds(randomInterval);
        }
    }

    private void FixedUpdate()
    {
        if(movementActive)
        {
            float xMoveSpeed = speed * Time.fixedDeltaTime;
            rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, xMoveSpeed);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag.Equals("Gate"))
        {
            onGateReached?.Invoke();
        }
    }
}
