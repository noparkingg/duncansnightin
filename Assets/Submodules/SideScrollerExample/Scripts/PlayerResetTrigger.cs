using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Resets the player to a specified point when they fall through the trigger
/// </summary>
public class PlayerResetTrigger : MonoBehaviour
{
    [SerializeField] Transform resetPoint;
    [SerializeField] GameObject player;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Player"))
        {
            other.gameObject.transform.position = resetPoint.position;
        }
    }
}
