using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static com.ImmersiveMedia.Enums.InteractionEnums;

/// <summary>
/// Holds a mapping from cursor to interaction type
/// </summary>
[Serializable]
public class CursorInteractionMapping
{
    [SerializeField] private Texture2D cursorImage;
    [SerializeField] private CharacterInteraction interaction;

    public Texture2D CursorImage { get => cursorImage;}
    public CharacterInteraction Interaction { get => interaction;}
}
