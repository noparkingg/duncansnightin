using com.ImmersiveMedia.CharacterControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static com.ImmersiveMedia.Enums.InteractionEnums;

/// <summary>
/// Controls a top down character
/// </summary>
public class TopDownCharacterController : IMNoncontinousInputCharacterController
{
    [SerializeField] Camera mainCamera;
    [SerializeField] CursorInteractionMap cursorMap; // The mapping of interactions to cursors stored as a scriptable object
    protected Texture2D cursorImage;

    /// <summary>
    /// Defines how the user selects an object for interaction
    /// </summary>
    protected override void InteractionSelectionLoop()
    {
        RaycastHit hit;

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;

            CharacterInteractable interactable = objectHit.gameObject.GetComponent<CharacterInteractable>();

            if (interactable != null)
            {
                interactable.InteractionPoint = hit.point;

                Texture2D reactableCursorImage = GetCursorImage(interactable.InteractionOption);
                // Change the cursor image to the interactables cursor image as it is hovered over
                if ((reactableCursorImage != null) && (cursorImage == null || !cursorImage.Equals(GetCursorImage(interactable.InteractionOption))))
                {
                    Cursor.SetCursor(reactableCursorImage, Vector2.zero, CursorMode.Auto);
                    cursorImage = reactableCursorImage;
                }

                // If the user clicks then start lerping to the destination the user clicked
                if(Input.GetMouseButtonDown(0) && interactable.InteractionOption != CharacterInteraction.NOT_INTERACTABLE)
                {
                    SetInteraction(interactable);   
                }
            }
        }
    }

    public Texture2D GetCursorImage(CharacterInteraction interaction)
    {
        return cursorMap.GetCursor(interaction);
    }
}
