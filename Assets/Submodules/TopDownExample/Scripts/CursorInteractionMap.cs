using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static com.ImmersiveMedia.Enums.InteractionEnums;

/// <summary>
/// Maps cursors to interactions
/// </summary>
[CreateAssetMenu(fileName = "CursorInteractionMap", menuName = "TopDownController/CursorInteractionMap")]
public class CursorInteractionMap : ScriptableObject
{
    [SerializeField] List<CursorInteractionMapping> mappingList;


    private Dictionary<CharacterInteraction, CursorInteractionMapping> cursorDict;

    public Texture2D GetCursor(CharacterInteraction interactionOption)
    {
        if(cursorDict == null)
        {
            IniatilizeDict();
        }

        if(cursorDict.ContainsKey(interactionOption))
        {
            return cursorDict[interactionOption].CursorImage;
        }

        Debug.LogError("No cursor has been mapped for action");
        return null;
    }

    public void IniatilizeDict()
    {
        cursorDict = new Dictionary<CharacterInteraction, CursorInteractionMapping>();

        foreach(var mapping in mappingList)
        {
            cursorDict.Add(mapping.Interaction, mapping);
        }
    }

    public List<CursorInteractionMapping> MappingList { get => mappingList; }
}
