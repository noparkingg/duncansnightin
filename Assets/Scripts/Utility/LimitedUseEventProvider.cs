using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class LimitedUseEventProvider : MonoBehaviour
{
    [SerializeField] private int remainingUses;

    [SerializeField] private UnityEvent<string> onSetRemainingUseAmount;

    [SerializeField] private UnityEvent onDeactivateEventProviders;
    [SerializeField] private UnityEvent onActivateEventProviders;
    [SerializeField] private bool depletionActive;

    [SerializeField] private bool regenerative;
    [SerializeField] private float regenerationInterval;
    [SerializeField] private int regenerationThreshold;

    private bool active = true;


    // Start is called before the first frame update
    void Start()
    {
        onSetRemainingUseAmount?.Invoke(remainingUses.ToString());
        StartCoroutine(RegenerationRoutine());
    }

    private IEnumerator RegenerationRoutine()
    {
        WaitForSeconds waitForSeconds = new WaitForSeconds(regenerationInterval);
        while(true)
        {
            yield return waitForSeconds;
            if (regenerative && remainingUses < regenerationThreshold)
            {
                AddMoreUses(1);
            }
            
        }
    }

    public void OnUse()
    {
        if(remainingUses > 0)
        {
            if(depletionActive)
            {
                remainingUses--;
            }

            onSetRemainingUseAmount?.Invoke(remainingUses.ToString());
        }
        

        if(remainingUses < 1 && active)
        {
            active = false;
            onDeactivateEventProviders?.Invoke();
        }
        onSetRemainingUseAmount?.Invoke(remainingUses.ToString());
    }

    public void AddMoreUses(int uses)
    {
        if(uses > 0)
        {
            remainingUses += uses;
            if(!active)
            {
                onActivateEventProviders.Invoke();
                active = true;
            }
            onSetRemainingUseAmount?.Invoke(remainingUses.ToString());
        }
    }

    public bool DepletionActive { get => depletionActive; set => depletionActive = value; }
}
