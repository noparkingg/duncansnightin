using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    [SerializeField] UnityEvent onTimerFinished;
    [SerializeField] UnityEvent<string> onTimerUpdate;
    [SerializeField] int maxTimeInSeconds;
    private int timeLeft;

    // Start is called before the first frame update
    void Start()
    {
        StartTimer();
    }

    public void StartTimer()
    {
        StartCoroutine(TimerLoop());
    }

    private IEnumerator TimerLoop()
    {
        timeLeft = maxTimeInSeconds;

        do
        {
            yield return new WaitForSeconds(1);
            timeLeft -= 1;
            onTimerUpdate?.Invoke(timeLeft + "");
        } while (timeLeft > 0);

        onTimerFinished?.Invoke();
    }
}
