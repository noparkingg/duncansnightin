using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    [SerializeField] float spinAmount;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, spinAmount * Time.deltaTime, 0);
    }
}
