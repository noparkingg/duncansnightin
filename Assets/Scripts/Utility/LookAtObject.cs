using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtObject : MonoBehaviour
{
    [SerializeField] Transform focusTransform;

    [SerializeField] bool lookAtX;
    [SerializeField] bool lookAtY;
    [SerializeField] bool lookAtZ;

    private Vector3 lookAtVector = Vector3.zero;

    // Update is called once per frame
    void Update()
    {
        if(focusTransform)
        {
            lookAtVector = new Vector3(focusTransform.position.x, focusTransform.position.y, focusTransform.position.z);

            transform.LookAt(lookAtVector);
        }
    }
}
