using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRCameraController : MonoBehaviour
{
    public static VRCameraController Instance;

    [SerializeField] private Transform head;

    [SerializeField] MeshRenderer rend;

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this);
        }
        Instance = this;
    }

    public void MoveCamera(Vector3 position)
    {
        Vector3 rootDistanceVector = new Vector3(head.transform.position.x, 0f, head.transform.position.z) -
            new Vector3(transform.position.x, 0f, transform.position.z);

        transform.position = new Vector3(position.x, position.y, position.z) - rootDistanceVector;

    }

    public void FadeOutCamera(float fadeStep)
    {
        StartCoroutine(FadeOutCamera(fadeStep, null));
    }

    public void FadeInCamera(float fadeStep)
    {
        StartCoroutine(FadeInCamera(fadeStep, null));
    }

    public IEnumerator FadeOutCamera(float fadeStep, Action completionCallback = null)
    {
        float alpha = 0;

        while(alpha < 1)
        {
            alpha += fadeStep;
            rend.material.color = new Color(0, 0, 0, alpha);
            yield return new WaitForUpdate();
        }

        completionCallback?.Invoke();
    }

    public IEnumerator FadeInCamera(float fadeStep, Action completionCallback = null)
    {
        float alpha = 1;

        while (alpha > 0)
        {
            alpha -= fadeStep;
            rend.material.color = new Color(0, 0, 0, alpha);
            yield return new WaitForUpdate();
        }

        completionCallback?.Invoke();
    }
}
