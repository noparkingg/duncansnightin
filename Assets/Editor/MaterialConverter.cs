using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

class MaterialConverter : EditorWindow
{
    [SerializeField] Material sourceMaterial;

    [MenuItem("Tools/MaterialConverter")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(MaterialConverter));
    }

    void OnGUI()
    {
        // The actual window code goes here

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Materials to convert");
        sourceMaterial = (Material) EditorGUILayout.ObjectField(sourceMaterial, typeof(Material), true);
        EditorGUILayout.EndHorizontal();

        if(GUILayout.Button("Convert"))
        {
            ConvertStandardMatToMobileDiffuse(sourceMaterial);
        }

        
    }

    private void ConvertStandardMatToMobileDiffuse(Material material)
    {
        Texture2D diffuseTexture = CreateTextureFromColor(material.color);
        byte[] bytes = diffuseTexture.EncodeToPNG();

        //Material newMat = new Material(Shader.Find("Mobile/Diffuse"));
        string materialPath = AssetDatabase.GetAssetPath(material);
        char[] charSeparators = new char[] { '.' };
        string texturePath = materialPath.Split(charSeparators)[0] + "_texture.png";

        string path = Application.dataPath.Replace("/Assets" , "") + "/" + texturePath;
        Debug.Log(path);
        File.WriteAllBytes(path, bytes);
        AssetDatabase.Refresh();
        material.shader = Shader.Find("Mobile/Diffuse");
        material.SetTexture("_MainTex", (Texture2D) AssetDatabase.LoadAssetAtPath(texturePath, typeof(Texture2D)));
        EditorUtility.SetDirty(material);
        AssetDatabase.SaveAssets();
    }

    private Texture2D CreateTextureFromColor(Color color)
    {
        var texture = new Texture2D(512, 512, TextureFormat.ARGB32, false);


        for(int i = 0; i < texture.height; i++)
        {
            for (int j = 0; j < texture.width; j++)
            {
                texture.SetPixel(i, j, color);
            }
        }
        // Apply all SetPixel calls
        texture.Apply();

        return texture;
    }
}
