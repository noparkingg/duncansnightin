using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class integrationCutscene : MonoBehaviour
{

    [SerializeField] string gameplay;
    [SerializeField] float cutSceneEnd = 55f;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(cutSceneEnd);
        SceneManager.LoadScene(gameplay, LoadSceneMode.Single);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
