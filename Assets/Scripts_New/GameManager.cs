using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;


public class GameManager : MonoBehaviour
{
   
    public static GameManager Instance;

    //good food is winCounter
    public int winCounter = 0;
    public int loseCounter = 0;


    [SerializeField] public GameObject GoodFood;
    [SerializeField] public GameObject BadFood;
    [SerializeField] public GameObject NightSaved;
    [SerializeField] public GameObject NightRuined;


    public GameState State;

    public static event Action<GameState>  onGameStateChanged;

    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.

        if (Instance == this)
        {
            Instance = this;
        }

        else
        {
            Destroy(this);
        }

    }

        void Start()
        {
        UpdateGameState(GameState.SeekFood);
        }

        public void UpdateGameState (GameState newState)
        {
            State = newState;

            switch (newState)
            {   
               
                case GameState.SeekFood:
                Deliberate();
                    break;
                case GameState.NightSaved:
                NightSavedScreen();
                    break;
                case GameState.NightRuined:
                NightRuinedScreen(); 
                    break;
                default:
                throw new ArgumentOutOfRangeException(nameof(newState), newState, null);

            }

            onGameStateChanged?.Invoke(newState);

        }

    public void NightRuinedScreen()
    {
        
        NightRuined.SetActive(true);

    }

    public void NightSavedScreen()
    {
        
        NightSaved.SetActive(true);
    }

    public enum GameState 
        {   SeekFood, 
            NightSaved,
            NightRuined
        }



    void Deliberate()
    {


        if (gameObject == GoodFood )
        {
            winCounter++;
            if (winCounter >= 15)
            {
                Debug.Log("You win!");
                UpdateGameState(GameState.NightSaved);
            }
        }

        if (gameObject == BadFood)
        {
            loseCounter++;
            if (loseCounter >= 3)
            {
                Debug.Log("You lose!");
                UpdateGameState(GameState.NightRuined);
            }
        }
    }




}

